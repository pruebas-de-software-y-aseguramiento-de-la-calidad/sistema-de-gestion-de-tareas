

<h2>Command line instructions</h2>

You can also upload existing files from your computer using the instructions below. <br>

<h3>Git global setup</h3>

git config --global user.name "UserName" <br>
git config --global user.email "correo.electronico@uniminuto.edu.co" <br>

<h3>Create a new repository</h3>

git clone https://gitlab.com/pruebas-de-software-y-aseguramiento-de-la-calidad/sistema-de-gestion-de-tareas.git <br>
cd sistema-de-gestion-de-tareas <br>
git switch --create main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push --set-upstream origin main <br>

<h3>Push an existing folder</h3>

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/pruebas-de-software-y-aseguramiento-de-la-calidad/sistema-de-gestion-de-tareas.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push --set-upstream origin main <br>

<h3>Push an existing Git repository</h3>

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/pruebas-de-software-y-aseguramiento-de-la-calidad/sistema-de-gestion-de-tareas.git <br>
git push --set-upstream origin --all <br>
git push --set-upstream origin --tags <br>

